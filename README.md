# ML_Project2

### Home Credit Default Risk

https://www.kaggle.com/c/home-credit-default-risk/data

### Структура проекта

* data: данные для проекта
* notebooks: ноутбуки по проекту

### Инструкция по запуску

В папке Data необходимы .csv файлы 
* application_train.csv
* application_test.csv
* sample_submission.csv
